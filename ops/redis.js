var redis = require('redis');

var redisClient = redis.createClient({host: "redis-master"});

redisClient.on('connect', function() {
	console.log('Redis client connected');
});

redisClient.on('error', function (err) {
	console.log('Something went wrong ' + err);
});

module.exports = redisClient;

