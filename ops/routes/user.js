var express = require('express');
var mongoose = require('mongoose');

var User = require('../model/user');
var router = express.Router();

router.post('/add', function(req, res, next) {
	var userInfo = req.body;

	if (!userInfo.name) {
		res.send("Error: Name is an essential field");	
	} else {
		var user = new User({
			_id: new mongoose.Types.ObjectId(),
			name: userInfo.name
		});
		user.save(function(err, doc) {
			if (err) {
				if (mongoose.connection.readyState == 0) {
					process.exit(1)
				}
				res.send(err);
			} else {

				console.log('User ' + doc.name + ' successfully saved');
				res.json(doc);
			}
		});
	}
});

router.post('/get', function(req, res, next) {
	var fetchCount = parseInt(req.body.count);
	if (!fetchCount || typeof fetchCount == 'undefined') {
		fetchCount = 10;
	} 
	User.find().limit(fetchCount).exec(function(err, doc) {
		if (err) {
			res.send(err);
		} else {
			console.log('Users  successfully fetched');
			res.json(doc);
		}
		
	});
	
});

module.exports = router;
