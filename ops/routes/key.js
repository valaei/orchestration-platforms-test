var express = require('express');
var redis = require('redis');

var redisClient = require('../redis');

var router = express.Router();

router.post('/set', function(req, res, next) {
	var keyInfo = req.body;

	if (!keyInfo.name || !keyInfo.value) {
		res.send("Error: Name and Value are essential fields");	
	} else {
		redisClient.set(keyInfo.name, keyInfo.value, function(err, result) {
			if (err) {
				res.send("Error: " + err);
			} else {
				res.json({
					"key": keyInfo.name,
					"value": keyInfo.value
				});
			}
		
		});
	}
});

router.post('/get', function(req, res, next) {
	var keyInfo = req.body;

	if (!keyInfo.name) {
		res.send("Error: Name is an essential field");	
	} else {
		redisClient.get(keyInfo.name, function(err, result) {
			if (err) {
				res.send("Error: " + err);
			} else {
				res.json({
					"key": keyInfo.name,
					"value": result
				});
			}
		
		});
	}
});

module.exports = router;
