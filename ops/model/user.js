var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	name: {
		type: String,
		required: true
	},
	created: { 
		type: Date,
		default: Date.now
	}
});

var User = mongoose.model("User", userSchema);

module.exports = User;
