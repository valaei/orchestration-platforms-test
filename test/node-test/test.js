  redis = 0
  mongo = 0

  module.exports = {
    checkResponse: checkResponse,
    checkResponse2: checkResponse2
  }
  
  function checkResponse(requestParams, response, context, ee, next) {
    if (response.body.value == 'true') {
      redis += 1
      console.log('Redis: ' + redis)
    }
    return next(); 
  }

  function checkResponse2(requestParams, response, context, ee, next) {
    if (response.body.name == 'test') {
      mongo += 1
      console.log('Mongo: ' + mongo)
    }
    return next(); 
  }
